#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo ""
echo "=== BeenUI runner ==="
echo ""
echo "Usage: ./startup.sh [port]"
echo ""
echo "====================="
echo ""
echo ""

if [ $# -eq 0 ]
then
    PORT=3000
    echo "PORT NOT SPECIFIED, USING $PORT"
    echo ""
  else
    PORT=$1
    echo "PORT SPECIFIED BY USER, USING $PORT"
    echo ""
fi

BUILD_DIR=build

JLHTTP_NAME=(`cd $DIR && ls jlhttp-*.jar`)
JLHTTP=$DIR/$JLHTTP_NAME

(cd $DIR && java -jar $JLHTTP $BUILD_DIR $PORT)
