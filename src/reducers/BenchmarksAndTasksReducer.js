import * as actions from "../constants/Actions";
import _ from "lodash";


function benchmarksAndTasksReducer(state = {tasks: [], benchmarks: []}, action) {
    let operation;
    switch (action.type) {
        case actions.TASKS_FETCHED:
            operation = _tasksFetched;
            break;
        case actions.BENCHMARKS_FETCHED:
            operation = _benchmarksFetched;
            break;
        case actions.BENCHMARK_FETCHED:
            operation = _benchmarkFetched;
            break;
        default:
            operation = _default;
    }
    return operation(state, action);
}

function _tasksFetched(state, action) {
    return {...state, tasks: action.tasks};
}

function _benchmarksFetched(state, action) {
    return {...state, benchmarks: action.benchmarks};
}

function _benchmarkFetched(state, action) {
    var updatedBench = action.benchmark;
    var benchmarkUuid = updatedBench.benchmarkUuid;

    var benchs = _.remove(state.benchmarks, function (o) {
        return o.benchmarkUuid !== benchmarkUuid;
    });

    benchs = [...benchs, updatedBench];
    return {...state, benchmarks: benchs};
}

function _default(state, action) {
    return {...state};
}

export default benchmarksAndTasksReducer;