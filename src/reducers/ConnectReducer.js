import * as actions from "../constants/Actions";

export default function connectReducer(state = {
    url: undefined,
    port: undefined,
    connected: false
}, action) {
    switch (action.type) {
        case actions.DISCONNECTING_FROM_BEEN:
            return {...state, url: undefined, port: undefined, connected: false}

        case actions.CONNECTION_SUCCESSFUL:
            return {...state, url: action.url, port: action.port, connected: true}

        case actions.CONNECTION_FAILED:
            return {...state, url: undefined, port: undefined, connected: false}

        default:
            return {...state};
    }
}