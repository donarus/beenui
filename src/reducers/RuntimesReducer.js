import * as actions from "../constants/Actions";

export default function runtimesReducer(state = {runtimes: []}, action) {
    switch (action.type) {
        case actions.RUNTIMES_UPDATED:
            return {...state, runtimes: action.runtimes}
        default:
            return {...state};
    }
}