import { combineReducers } from 'redux'
import logsReducer from './LogsReducer'
import softwareRepositoryReducer from './SoftwareRepositoryReducer'
import connectToBeenReducer from './ConnectReducer'
import runtimesReducer from './RuntimesReducer'
import benchmarksAndTasksReducer from './BenchmarksAndTasksReducer'
import clusterReducer from './ClusterReducer'

const rootReducer = combineReducers({
    logs: logsReducer,
    connection: connectToBeenReducer,
    softwareRepository: softwareRepositoryReducer,
    runtimes: runtimesReducer,
    benchmarksAndTasks: benchmarksAndTasksReducer,
    cluster: clusterReducer
})

export default rootReducer