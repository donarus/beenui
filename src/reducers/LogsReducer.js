import * as actions from "../constants/Actions";

export default function logsReducer(state = {logs: []}, action) {
    switch (action.type) {
        case actions.LOG_MESSAGES_RECEIVED:
            return {...state, logs: [...state.logs, ...action.logs]}

        default:
            return {...state};
    }
}