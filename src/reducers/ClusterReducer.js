import * as actions from "../constants/Actions";

const _defaultState = {nodes: [], services: []}

function clusterReducer(state = _defaultState, action) {
    let operation;
    switch (action.type) {
        case actions.CLUSTER_DATA_INIT:
            operation = _clusterDataInit;
            break;
        case actions.CLUSTER_NODE_ADDED:
            operation = _clusterNodeAdded;
            break;
        case actions.CLUSTER_NODE_REMOVED:
            operation = _clusterNodeRemoved;
            break;
        default:
            operation = _default;
    }
    return operation(state, action);
}

function _clusterDataInit(state, action) {
    return {...state, nodes: action.nodes, services: action.services};
}

function _clusterNodeAdded(state, action) {
    return {...state, nodes: [...state.nodes, action.node]};
}

function _clusterNodeRemoved(state, action) {
    let nodes = state.nodes;
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].nodeUuid === action.nodeUuid) {
            nodes = _removeItem(nodes, i);
            break;
        }
    }
    return {...state, nodes: nodes};
}

function _removeItem(data, index) {
    return [...data.slice(0, index), ...data.slice(index + 1)]
}

function _default(state, action) {
    return {...state};
}

export default clusterReducer;