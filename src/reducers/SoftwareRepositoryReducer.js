import * as actions from "../constants/Actions";

const _defaultState = {items: []};

function softwareRepositoryReducer(state = _defaultState, action) {
    let operation;
    switch (action.type) {
        case actions.BPKS_FETCHED:
            operation = _bpksFetched; break;
        case actions.BPK_UPLOADED:
            operation = _bpkUploaded; break;
        case actions.BPK_DELETED:
            operation = _bpkDeleted; break;
        default:
            operation = _default;
    }
    return operation(state, action);
}

function _default(state, action) {
    return {...state};
}

function _bpksFetched(state, action) {
    return {...state, items: action.items};
}

function _bpkUploaded(state, action) {
    var items = [...state.items];
    let alreadyExist = false;
    for (var i = 0; i < items.length; i++) {
        if (items[i].groupId === action.groupId
            && items[i].artifactId === action.artifactId
            && items[i].version === action.version) {
            alreadyExist = true;
            break;
        }
    }
    if (!alreadyExist) {
        items = [...items, {groupId: action.groupId, artifactId: action.artifactId, version: action.version}];
    }
    return {...state, items: items};
}

function _bpkDeleted(state, action) {
    var items = [...state.items];
    for (var i = 0; i < items.length; i++) {
        if (items[i].groupId === action.groupId
            && items[i].artifactId === action.artifactId
            && items[i].version === action.version) {
            items.splice(i, 1);
        }
    }
    return {...state, items: items};
}

export default softwareRepositoryReducer;