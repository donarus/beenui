import SockJS from "sockjs-client";
import Webstomp from "webstomp-client";
import {beenApi} from "./BeenApi";

export default class BeenWebSocket {

    _stomp = null;

    _actualTopicIds = {};
    _actualTopicNames = {};
    _actualTopicHandlers = {};

    connect(socketUrl, successCallback, errorCallback) {
        this._stomp = Webstomp.over(new SockJS(socketUrl));
        this._stomp.connect(
            {/*headers*/},
            (frame) => {
                successCallback(frame);

                for (let topicName of Object.keys(this._actualTopicNames)) {
                    this.joinTopic(topicName, this._actualTopicHandlers[topicName])
                }
            },
            errorCallback
        );
    }

    disconnect() {
        for (let topicName of Object.keys(this._actualTopicNames)) {
            this.leaveTopic(topicName)
        }
        this._stomp.disconnect();
        this._stomp = null;
        this._actualTopicIds = {};
        this._actualTopicNames = {};
        this._actualTopicHandlers = {};
    }

    joinTopic(topicName, topicHandler) {
        if (this._stomp) {
            const topic = this._stomp.subscribe("/topic/" + topicName, function (response, headers) {
                const msg = JSON.parse(response.body);
                topicHandler(msg.data);
                beenApi.alertMessages(msg.messages);
            });
            this._actualTopicIds[topicName] = topic.id;
        }
        this._actualTopicNames[topicName] = topicName;
        this._actualTopicHandlers[topicName] = topicHandler;
    }

    leaveTopic(topicName) {
        if (this._stomp) {
            this._stomp.unsubscribe(this._actualTopicIds[topicName]);
        }
        delete this._actualTopicNames[topicName];
        delete this._actualTopicHandlers[topicName];
        delete this._actualTopicIds[topicName];
    }

}