import axios from "axios";
import BeenWebSocket from "./BeenWebSocket";
import * as actions from "../constants/Actions";
import cookie from "react-cookie";
import Alert from "react-s-alert";


class BeenApi {

    BEEN_MAGIC_PING_SEQUENCE = "PING to running Been successful!";

    /**
     * redux store set by user after creating it
     * @type {Store}
     * @private
     */
    _store = null;

    /**
     * Url of running Been instance set in connect method
     * @type {string}
     * @private
     */
    _url = null;

    /**
     * Port of running Been instance set in connect method
     * @type {integer}
     * @private
     */
    _port = null;

    /**
     * BeenWebSocket instance connected to Been running instance in connect method
     * @type {BeenWebSocket}
     * @private
     */
    _beenWS = new BeenWebSocket();

    tryInit() {
        const [storedUrl, storedPort] = this._loadBeenApiCookies();
        if (storedUrl) {
            this.connect(storedUrl, storedPort, true);
        }
    }

    /**
     * Connect to Been instance on given url and port and prepare and
     * open websocket for further use.
     * @param url
     * @param port
     * @param autoinit if set to true, don't alert messages when connection is not successfull
     */
    connect(url, port, autoinit = false) {
        Alert.closeAll();

        this._url = this._sanitizeBaseUrl(url);
        this._port = port;

        if (url === "") {
            if (!autoinit) {
                this.alertMessages([{logLevel: "WARN", message: "Empty url not allowed"}]);
            }
            return;
        }

        axios.get(this.getPingUrl()).then((response) => {
            if (response.data === this.BEEN_MAGIC_PING_SEQUENCE) {
                this._connectBeenWebSocket();
                this.alertMessages([{logLevel: "INFO", message: "Successfully connected"}]);
            } else {
                const error = "Response status from PING message is 2xx but response body does not match expected text";
                if (!autoinit) {
                    this.alertMessages([{logLevel: "ERROR", message: "Connection failed: " + error}]);
                }
                console.err(error);
            }
        }).catch(error => {
            if (!autoinit) {
                this.alertMessages([{
                    logLevel: "ERROR",
                    message: "Connection failed, check URL and check that cluster is running: " + error
                }]);
            }
            console.error(error);
        });
    }

    disconnect() {
        Alert.closeAll();
        this._url = null;
        this._port = null;
        this._beenWS.disconnect();
        this._deleteBeenApiCookies();
        this._dispatch({
            type: actions.DISCONNECTING_FROM_BEEN
        })
        this.alertMessages([{logLevel: "INFO", message: "Logout successfull"}]);
    }

    getBeenWebSocket() {
        return this._beenWS;
    }

    //
    // INFRASTRUCTURE URLS
    //
    getPingUrl() {
        return this._constructUrl('public/global/ping');
    }

    //
    // TASKS URLS
    //
    getTasksRunUrl() {
        return this._constructUrl('public/tasks/run');
    }

    getTasksListUrl() {
        return this._constructUrl('public/tasks/list');
    }

    getBenchmarksListUrl() {
        return this._constructUrl('public/tasks/listBenchmarks');
    }


    getTaskKillUrl(taskId) {
        const deletePart = `public/tasks/kill?taskId=${taskId}`;
        return this._constructUrl(deletePart);
    }

    getTaskContextKillUrl(taskContextId) {
        const deletePart = `public/tasks/killTaskContext?taskContextId=${taskContextId}`;
        return this._constructUrl(deletePart);
    }

    getBenchmarkKillUrl(benchmarkId) {
        const deletePart = `public/tasks/killBenchmark?benchmarkId=${benchmarkId}`;
        return this._constructUrl(deletePart);
    }

    getTaskDetailUrl(taskId) {
        return this._constructUrl(`public/tasks/detail?taskId=${taskId}`);
    }

    getBenchmarkDetailUrl(benchmarkId) {
        return this._constructUrl(`public/tasks/getBenchmarkDetail?benchmarkId=${benchmarkId}`);
    }

    getTaskLogs(taskId) {
        return this._constructUrl(`public/tasks/logs?taskId=${taskId}`);
    }

    getTasksResultDownloadUrl(taskId) {
        const getPart = `public/tasks/getResults?taskId=${taskId}`;
        return this._constructUrl(getPart);
    }

    //
    // SWR URLS
    //
    getSWRListUrl() {
        return this._constructUrl('public/swr/list');
    }

    getSWRGetUrl(groupId, artifactId, version) {
        const getPart = `public/swr/get?groupId=${groupId}&artifactId=${artifactId}&version=${version}`;
        return this._constructUrl(getPart);
    }

    getSWRPutUrl() {
        return this._constructUrl('public/swr/upload');
    }

    getSWRGetBpkDescriptorUrl(groupId, artifactId, version) {
        const getPart = `public/swr/getBpkDescriptor?groupId=${groupId}&artifactId=${artifactId}&version=${version}`;
        return this._constructUrl(getPart);
    }

    getSWRDeleteUrl(groupId, artifactId, version) {
        const deletePart = `public/swr/delete?groupId=${groupId}&artifactId=${artifactId}&version=${version}`;
        return this._constructUrl(deletePart);
    }

    //
    // RUNTIMES URLS
    //
    getRuntimesListUrl() {
        return this._constructUrl('public/runtimes/list');
    }

    //
    // CLUSTER URLS
    //
    getClusterDataInitUrl() {
        return this._constructUrl('public/cluster/data');
    }

    getClusterNodeDetailUrl(nodeUuid) {
        return this._constructUrl(`public/cluster/nodeDetail?nodeUuid=${nodeUuid}`);
    }

    getClusterServiceDetailUrl(serviceUuid) {
        return this._constructUrl(`public/cluster/serviceDetail?serviceUuid=${serviceUuid}`);
    }

    //
    // LOGS url
    //
    getClusterLogsUrl() {
        return this._constructUrl('public/logs/clusterLogs');
    }

    //
    // WEBSOCKET URLS
    //
    getWebsocketUrl() {
        return this._constructUrl('public/websocket');
    }

    setReduxStore(store) {
        this._store = store;
    }

    _connectBeenWebSocket() {
        this._beenWS.connect(
            this.getWebsocketUrl(), // web socket url
            (frame) => { // success callback
                this._dispatch({
                    type: actions.CONNECTION_SUCCESSFUL,
                    url: this._url,
                    port: this._port
                });
                this._saveBeenApiCookies();
            },
            (frame) => { // error callback
                this.alertMessages([{logLevel: "ERROR_NODISMISS", message: "Been disconnected!!!"}]);
            }
        )
    }

    _saveBeenApiCookies() {
        cookie.save('beenUrl', this._url, {path: '/'});
        cookie.save('beenPort', this._port, {path: '/'});
    }

    _deleteBeenApiCookies() {
        cookie.remove('beenUrl', {path: '/'});
        cookie.remove('beenPort', {path: '/'});
    }

    _loadBeenApiCookies() {
        return [cookie.load('beenUrl'), cookie.load('beenPort')];
    }

    _sanitizeBaseUrl(baseUrl) {
        if (!baseUrl.startsWith("http://") && !baseUrl.startsWith("https://")) {
            return "http://" + baseUrl;
        }
        return baseUrl;
    }

    _constructUrl(endpoint) {
        return `${this._url}:${this._port}/${endpoint}`;
    }

    _dispatch(event) {
        this._store.dispatch(event);
    }

    joinTopic(topicName, dataHandler, initialDataUrl = undefined) {
        if (initialDataUrl) {
            this.getData(dataHandler, initialDataUrl);
        }
        this.getBeenWebSocket().joinTopic(topicName, dataHandler);
    }

    getData(dataHandler, dataUrl, successMessage = null, errorMessage = null) {
        axios.get(dataUrl).then((response) => {
            dataHandler(response.data.data);
            let messages = response.data.messages;
            if (successMessage) {
                messages = [...messages, {logLevel: "INFO", message: successMessage}];
            }
            this.alertMessages(messages);
        }).catch(error => {
            let finalMsg = "";
            if (errorMessage != null) {
                finalMsg = errorMessage;
            }
            finalMsg = `${finalMsg} ${error}`;
            console.error(finalMsg);
        });
    }

    postData(dataHandler, dataUrl, data, successMessage = null, errorMessage = null) {
        axios.post(dataUrl, data).then((response) => {
            dataHandler(response.data.data);
            let messages = response.data.messages;
            if (successMessage) {
                messages = [...messages, {logLevel: "INFO", message: successMessage}]
            }
            this.alertMessages(messages)
        }).catch(error => {
            let finalMsg = "";
            if (errorMessage != null) {
                finalMsg = errorMessage;
            }
            finalMsg = `${finalMsg}. ${error}`;
            console.error(finalMsg);
        });
    }

    deleteData(dataHandler, dataUrl, successMessage = null, errorMessage = null) {
        axios.delete(dataUrl).then((response) => {
            dataHandler(response.data.data);
            let messages = response.data.messages;
            if (successMessage) {
                messages = [...messages, {logLevel: "INFO", message: successMessage}];
            }
            this.alertMessages(messages)
        }).catch(error => {
            let finalMsg = "";
            if (errorMessage != null) {
                finalMsg = errorMessage;
            }
            finalMsg = `${finalMsg}. ${error}`;
            console.error(finalMsg);
        });
    }

    alertMessages(messages) {
        if (messages != null) {
            for (let msg of messages) {
                if (msg.logLevel === "ERROR") {
                    Alert.error(msg.message, {timeout: 15000});
                } else if (msg.logLevel === "WARN") {
                    Alert.warning(msg.message, {timeout: 10000});
                } else if (msg.logLevel === "INFO") {
                    Alert.success(msg.message);
                }
            }
        }
    }

    leaveTopic(topicName) {
        this.getBeenWebSocket().leaveTopic(topicName);
    }
}


export let beenApi = new BeenApi();