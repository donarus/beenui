import React, {Component} from "react";
import Header from "./components/HeaderComponent";
import {connect} from "react-redux";
import {Grid, Row, Col} from "react-bootstrap";
import {bindActionCreators} from "redux";
import {beenApi} from "./services/BeenApi";
import Alert from 'react-s-alert';

import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';

class App extends Component {

    render() {
        return (
            <div>
                <Header connected={this.props.connected} logoutAction={() => beenApi.disconnect()}/>

                <Grid>
                    <Row>
                        <Col>{this.props.children}</Col>
                    </Row>
                </Grid>
                <Alert stack={{limit: 10, spacing: 5}} position="bottom" timeout={5000} html={false} />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        connected: state.connection.connected,
        beenUrl: state.connection.url,
        beenPort: state.connection.port
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);