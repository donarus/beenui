import * as actions from "../constants/Actions";

export function runtimesUpdatedHandler(runtimesData) {
    return function (dispatch) {
        dispatch({
            type: actions.RUNTIMES_UPDATED,
            runtimes: runtimesData
        })
    }
}