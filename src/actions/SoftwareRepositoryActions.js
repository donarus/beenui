import axios from "axios";
import * as actions from "../constants/Actions";
import {beenApi} from "../services/BeenApi";
import Alert from 'react-s-alert';

export function packagesUpdatedHandler(packagesData) {
    return function (dispatch) {
        dispatch({
            type: actions.BPKS_FETCHED,
            items: packagesData
        })
    }
}

export function packageUploadedHandler(groupId, artifactId, version) {
    return function (dispatch) {
        dispatch({
            type: actions.BPK_UPLOADED,
            groupId: groupId,
            artifactId: artifactId,
            version: version
        })
    }
}

export function deleteBpk(groupId, artifactId, version) {
    return function (dispatch) {
        return axios.delete(beenApi.getSWRDeleteUrl(groupId, artifactId, version)).then(() => {
            dispatch({
                type: actions.BPK_DELETED,
                groupId: groupId,
                artifactId: artifactId,
                version: version
            })
            Alert.success(`BPK ${groupId}:${artifactId}:${version} successfully deleted`);
        }).catch(error => {
            Alert.error(`BPK ${groupId}:${artifactId}:${version} could not be deleted deleted. ` + error)
        });
    };
}