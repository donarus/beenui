import * as actions from "../constants/Actions";

export function clusterTopicHandler(data) {
    console.log(data)
    return function (dispatch) {
        if (data.type === "DATA_INIT") {
            dispatch({
                type: actions.CLUSTER_DATA_INIT,
                nodes: data.nodes,
                services: data.services
            });
        } else if (data.type === "NODE_ADDED") {
            dispatch({
                type: actions.CLUSTER_NODE_ADDED,
                node: data.node
            });
        } else if (data.type === "NODE_REMOVED") {
            dispatch({
                type: actions.CLUSTER_NODE_REMOVED,
                nodeUuid: data.nodeUuid
            });
        }
    }
}