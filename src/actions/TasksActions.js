import * as actions from "../constants/Actions";

export function tasksUpdatedHandler(tasks) {
    return function (dispatch) {
        dispatch({
            type: actions.TASKS_FETCHED,
            tasks: tasks
        })
    }
}
export function benchmarksUpdatedHandler(benchmarks) {
    return function (dispatch) {
        dispatch({
            type: actions.BENCHMARKS_FETCHED,
            benchmarks: benchmarks
        })
    }
}
export function benchmarkFetchedHandler(benchmark) {
    return function (dispatch) {
        dispatch({
            type: actions.BENCHMARK_FETCHED,
            benchmark: benchmark
        })
    }
}