import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {Router, browserHistory} from "react-router";
import configureStore from "./store/configureStore";
import routes from "./routes";
import {beenApi} from "./services/BeenApi"

const store = configureStore();
beenApi.setReduxStore(store);
beenApi.tryInit();

ReactDOM.render(
    <Provider store={store}><Router history={browserHistory} routes={routes}/></Provider>,
    document.getElementById('root')
);
