import React from "react";
import {Grid} from "react-bootstrap";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {beenApi} from "../services/BeenApi";
import Spinner from "../components/Spinner";
import PackageDetailComponent from "../components/packages/PackageDetail";

class PackageDetail extends React.Component {

    static contextTypes = {
        router: React.PropTypes.object.isRequired
    }

    constructor(options) {
        super(options)
        const {groupId, artifactId, version} = options.params
        this.state = {groupId, artifactId, version}
    }

    onRunTaskAsIs(bpkDescriptor, taskDescriptor) {
        const runTaskMessage = {
            bpkId: bpkDescriptor.bpkId,
            taskDescriptor: taskDescriptor
        }
        beenApi.postData(
            (response) => {
                 this.context.router.push(`/benchmarks-and-tasks`)
            },
            beenApi.getTasksRunUrl(),
            runTaskMessage
        )
    }

    render() {

        if (!this.state.bpkDescriptor) {
            return (<Spinner/>);
        }

        return (
            <Grid>
                <PackageDetailComponent
                    bpkDescriptor={this.state.bpkDescriptor}
                    onRunTaskAsIs={(taskDescriptor) => this.onRunTaskAsIs(this.state.bpkDescriptor, taskDescriptor)}/>
            </Grid>
        )
    }

    componentDidMount() {
        const {groupId, artifactId, version} = this.state
        beenApi.getData(
            (bpkDescriptor) => this.setState({bpkDescriptor: bpkDescriptor}),
            beenApi.getSWRGetBpkDescriptorUrl(groupId, artifactId, version)
        );
    }

}

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PackageDetail);