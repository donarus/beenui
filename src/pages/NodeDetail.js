import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {beenApi} from "../services/BeenApi";
import Spinner from "../components/Spinner";
import ClusterNodeDetail from "../components/cluster/ClusterNodeDetail";

class NodeDetail extends React.Component {
    static contextTypes = {
        router: React.PropTypes.object.isRequired
    }

    constructor(options) {
        super(options)
        this.state = {nodeUuid: options.params.nodeUuid};
    }

    render() {
        if (!this.state.node) {
            return (<Spinner/>);
        }

        return (
            <ClusterNodeDetail node={this.state.node}
            onShowServiceDetail={(serviceUuid) => this.onShowServiceDetail(serviceUuid)}/>
        )
    }

    componentDidMount() {
        beenApi.getData(
            (data) => this.setState({node: data.node}),
            beenApi.getClusterNodeDetailUrl(this.state.nodeUuid)
        );
    }


    onShowServiceDetail(serviceUuid) {
        this.context.router.push(`/cluster/service/${serviceUuid}`)
    }

}

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NodeDetail);