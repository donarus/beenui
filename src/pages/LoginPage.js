import React, {Component} from "react";
import {Grid, FormGroup, FormControl, Button, Row, Col, Form, Panel} from "react-bootstrap";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {beenApi} from "../services/BeenApi";

class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.state = {url: "", port:80};
    }

    onSubmit() {
        return (e) => {
            e.preventDefault();
            beenApi.connect(this.state.url, this.state.port);
        }
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col lg={5} lgOffset={4}>
                        <Panel>
                            {this.props.connected
                                ?
                                <p>Already connected</p>
                                :
                                <Form inline onSubmit={this.onSubmit()}>
                                    <FormGroup controlId="formBasicText">
                                        <FormControl type="text" placeholder="Enter been url" value={this.state.url}
                                                     onChange={(event) => this.setState({...this.state, url: event.target.value})}/>
                                        <FormControl type="number" placeholder="Enter been port" value={this.state.port}
                                                     onChange={(event) => this.setState({...this.state, port: event.target.value})}/>
                                        {' '}
                                        <Button onClick={this.onSubmit()}>Connect</Button>
                                    </FormGroup>
                                </Form>
                            }
                        </Panel>
                    </Col>
                </Row>

                <Row>
                    <div className="text-center col-md-4 col-md-offset-4 beenlogo"></div>
                </Row>
            </Grid>
        );
    }
}

function mapStateToProps(state) {
    return {connected: state.connection.connected};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);