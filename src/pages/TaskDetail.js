import React from "react";
import {Grid} from "react-bootstrap";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {beenApi} from "../services/BeenApi";
import Spinner from "../components/Spinner";
import TaskDetailComponent from "../components/benchmarks_and_tasks/TaskDetail";

class TaskDetail extends React.Component {

    static contextTypes = {
        router: React.PropTypes.object.isRequired
    }

    constructor(options) {
        super(options)
        const {taskId} = options.params
        this.state = {taskId}
    }


    onShowNodeDetail(nodeId) {
        this.context.router.push(`/cluster/node/${nodeId}`)
    }


    onShowBenchmarkDetail(benchmarkUuid) {
        this.context.router.push(`/benchmark/${benchmarkUuid}`)
    }

    onCreateDownloadResultsUrl(taskId) {
        return beenApi.getTasksResultDownloadUrl(taskId)
    }

    render() {
        if (!this.state.task) {
            return (<Spinner/>);
        }

        return (
            <Grid>
                <TaskDetailComponent
                    task={this.state.task}
                    logs={this.state.logs}
                    onShowNodeDetail={(id) => this.onShowNodeDetail(id)}
                    onShowBenchmarkDetail={(id) => this.onShowBenchmarkDetail(id)}
                    onCreateDownloadResultsUrl={this.onCreateDownloadResultsUrl}/>
            </Grid>
        )
    }

    componentDidMount() {
        const {taskId} = this.state
        beenApi.getData(
            (task) => this.setState({task: task}),
            beenApi.getTaskDetailUrl(taskId)
        );

        beenApi.getData(
            (logs) => this.setState({logs: logs}),
            beenApi.getTaskLogs(taskId)
        );
    }

}

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskDetail);