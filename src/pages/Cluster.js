import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import ClusterNodes from "../components/cluster/ClusterNodes";
import ClusterServices from "../components/cluster/ClusterServices";
import {beenApi} from "../services/BeenApi";
import {clusterTopicHandler} from "../actions/ClusterActions";
import {Tabs, Tab} from "react-bootstrap";

class Cluster extends React.Component {
    static contextTypes = {
        router: React.PropTypes.object.isRequired
    }

    onShowNodeDetail(nodeUuid) {
        this.context.router.push(`/cluster/node/${nodeUuid}`)
    }

    onShowServiceDetail(serviceUuid) {
        this.context.router.push(`/cluster/service/${serviceUuid}`)
    }

    componentDidMount() {
        beenApi.joinTopic("cluster", this.props.clusterTopicHandler, beenApi.getClusterDataInitUrl());
    }

    componentWillUnmount() {
        beenApi.leaveTopic("cluster");
    }

    render() {
        return (
            <Tabs id="tabs">
                <Tab eventKey={1} title="Connected nodes">
                    <ClusterNodes
                        nodes={this.props.nodes}
                        onShowDetail={(nodeUuid) => this.onShowNodeDetail(nodeUuid)}
                        onShowServiceDetail={(serviceUuid) => this.onShowServiceDetail(serviceUuid)}/>
                </Tab>
                <Tab eventKey={2} title="Running services">
                    <ClusterServices
                        services={this.props.services}
                        onShowDetail={(serviceUuid) => this.onShowServiceDetail(serviceUuid)}
                        onShowNodeDetail={(nodeUuid) => this.onShowNodeDetail(nodeUuid)}/>
                </Tab>
            </Tabs>
        );
    }
}

export default connect(
    (state) => {
        return {
            nodes: state.cluster.nodes,
            services: state.cluster.services
        }
    },
    (dispatch) => {
        return bindActionCreators({clusterTopicHandler}, dispatch);
    }
)(Cluster);