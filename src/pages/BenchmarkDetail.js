import React from "react";
import {Grid} from "react-bootstrap";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {beenApi} from "../services/BeenApi";
import {benchmarkFetchedHandler} from "../actions/TasksActions";
import Spinner from "../components/Spinner";
import BenchmarkDetailComponent from "../components/benchmarks_and_tasks/BenchmarkDetail";
import _ from "lodash";

class BenchmarkDetail extends React.Component {

    static contextTypes = {
        router: React.PropTypes.object.isRequired
    }

    constructor(options) {
        super(options)
        const {benchmarkId} = options.params
        this.state = {benchmarkId, isFetching: false}
    }

    getBenchmark() {
        if (this.props.benchmarks) {
            return _.find(this.props.benchmarks, (o) => o.benchmarkUuid === this.state.benchmarkId);
        }
        return null;
    }


    onKillTask(taskId) {
        beenApi.deleteData(
            (response) => { /* handler is mandatory */
            },
            beenApi.getTaskKillUrl(taskId)
        )
    }

    onKillTaskContext(taskContextId) {
        beenApi.deleteData(
            (response) => { /* handler is mandatory */
            },
            beenApi.getTaskContextKillUrl(taskContextId)
        )
    }

    onKillBenchmark(benchmarkId) {
        beenApi.deleteData(
            (response) => { /* handler is mandatory */
            },
            beenApi.getBenchmarkKillUrl(benchmarkId)
        )
    }

    onShowTaskDetail(taskId) {
        this.context.router.push(`/task/${taskId}`)
    }


    render() {
        const benchmark = this.getBenchmark();

        if (!benchmark) {
            return (<Spinner/>);
        }

        return (
            <Grid>
                <BenchmarkDetailComponent
                    benchmark={benchmark}
                    onKillTask={(taskId) => this.onKillTask(taskId)}
                    onKillTaskContext={(taskContextId) => this.onKillTaskContext(taskContextId)}
                    onKillBenchmark={(benchmarkId) => this.onKillBenchmark(benchmarkId)}
                    onShowTaskDetail={(taskId) => this.onShowTaskDetail(taskId)}
                />
            </Grid>
        )
    }

    // I know, I know, long polling is evil, but there is no ti
    componentDidMount() {
        this.dataFetch();
        this.interval = setInterval(() => this.dataFetch(), 3000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    dataFetch() {
        if (!this.state.isFetching) {
            this.setState({...this.state, isFetching: true});
            beenApi.getData((data) => {
                this.props.benchmarkFetchedHandler(data);
                this.setState({...this.state, isFetching: false});
            }, beenApi.getBenchmarkDetailUrl(this.state.benchmarkId));
        }
    }
}

function mapStateToProps(state) {
    return {
        benchmarks: state.benchmarksAndTasks.benchmarks
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({benchmarkFetchedHandler}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BenchmarkDetail);