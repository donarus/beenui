import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {beenApi} from "../services/BeenApi";
import Spinner from "../components/Spinner";
import ClusterServiceDetail from "../components/cluster/ClusterServiceDetail";

class ServiceDetail extends React.Component {
    static contextTypes = {
        router: React.PropTypes.object.isRequired
    }

    constructor(options) {
        super(options)
        this.state = {serviceUuid: options.params.serviceUuid};
    }

    render() {
        if (!this.state.service) {
            return (<Spinner/>);
        }

        return (
            <ClusterServiceDetail service={this.state.service}
                                  onShowNodeDetail={(nodeUuid) => this.onShowNodeDetail(nodeUuid)}/>
        )
    }

    componentDidMount() {
        beenApi.getData(
            (data) => this.setState({service: data.service}),
            beenApi.getClusterServiceDetailUrl(this.state.serviceUuid)
        );
    }

    onShowNodeDetail(nodeUuid) {
        this.context.router.push(`/cluster/node/${nodeUuid}`)
    }

}

function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceDetail);