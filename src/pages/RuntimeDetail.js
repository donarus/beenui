import React from "react";
import {connect} from "react-redux";

class RuntimeDetail extends React.Component {
    render() {
        return (
            <p>Runtime detail page...</p>
        );
    }
}

export default connect(
    (state) => {
        return {
            connected: state.connection.connected
        }
    },
    (dispatch) => {
        return {};
    }
)(RuntimeDetail);