import React, {Component} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {deleteBpk, packagesUpdatedHandler, packageUploadedHandler} from "../actions/SoftwareRepositoryActions";
import {beenApi} from "../services/BeenApi";
import PackagesPanel from "../components/packages/PackagesPanel";
import UploadNewPackage from "../components/packages/UploadNewPackage";

class Packages extends Component {
    static contextTypes = {
        router: React.PropTypes.object.isRequired
    }

    onCreateDownloadUrl(groupId, artifactId, version) {
        return beenApi.getSWRGetUrl(groupId, artifactId, version)
    }

    onShowDetail(groupId, artifactId, version) {
        this.context.router.push(`/package/${groupId}/${artifactId}/${version}`)
    }

    render() {
        return (
            <div>
                <PackagesPanel
                    packages={this.props.items}
                    onDelete={this.props.deleteBpk}
                    onCreateDownloadUrl={this.onCreateDownloadUrl}
                    onShowDetail={(groupId, artifactId, version) => this.onShowDetail(groupId, artifactId, version)}/>

                <UploadNewPackage onUpload={this.props.packageUploadedHandler} uploadUrl={beenApi.getSWRPutUrl()}/>
            </div>
        );
    }

    componentDidMount() {
        beenApi.getData(this.props.packagesUpdatedHandler, beenApi.getSWRListUrl());
    }
}

function mapStateToProps(state) {
    return {
        items: state.softwareRepository.items
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({deleteBpk, packagesUpdatedHandler, packageUploadedHandler}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Packages);