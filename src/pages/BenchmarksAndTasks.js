import React, {Component} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {benchmarksUpdatedHandler} from "../actions/TasksActions";
import {beenApi} from "../services/BeenApi";
import BenchmarksPanel from "../components/benchmarks_and_tasks/BenchmarksPanel";

class BenchmarksAndTasks extends Component {
    static contextTypes = {
        router: React.PropTypes.object.isRequired
    }

    onKillBenchmark(benchmarkId) {
        beenApi.deleteData(
            (response) => { /* handler is mandatory */
            },
            beenApi.getBenchmarkKillUrl(benchmarkId)
        )
    }

    onShowBenchmarkDetail(benchmarkUuid) {
        this.context.router.push(`/benchmark/${benchmarkUuid}`)
    }

    render() {
        return (

            <BenchmarksPanel
                benchmarks={this.props.benchmarks}
                onKillBenchmark={(benchmarkUuid) => this.onKillBenchmark(benchmarkUuid)}
                onShowBenchmarkDetail={(benchmarkUuid) => this.onShowBenchmarkDetail(benchmarkUuid)}/>
        )


            // <TasksPanel
            //     tasks={this.props.items}
            //     onKill={(taskId) => this.onKill(taskId)}
            //     onShowTaskDetail={(taskId) => this.onShowTaskDetail(taskId)}/>
    }

    componentDidMount() {
        beenApi.getData(this.props.benchmarksUpdatedHandler, beenApi.getBenchmarksListUrl());
    }
}

function mapStateToProps(state) {
    return {
        benchmarks: state.benchmarksAndTasks.benchmarks
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({benchmarksUpdatedHandler}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BenchmarksAndTasks);