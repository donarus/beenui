import React from "react";


class Common {
    colorizeTaskStatus(status) {
        let color;
        switch (status) {
            case "RUNNING":
                color = "blue";
                break;
            case "FAILED":
                color = "red";
                break;
            case "PLANNED":
                color = "orange";
                break;
            case "FINISHED":
                color = "green";
                break;
            default:
                color = "black"
        }
        return (<strong className={color}>{status}</strong>)
    }

    toDateString(val) {
        if (val) {
            return new Date(val).toLocaleString()
        } else {
            return "";
        }
    }
}


export let common = new Common();