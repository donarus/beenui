import React from "react";

export default class Scrollable extends React.Component {
    render() {
        return <div style={{overflow: "auto", height: this.props.height}}>{this.props.children}</div>
    }
}