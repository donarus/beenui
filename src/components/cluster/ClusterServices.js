import React from "react";
import {Table, Glyphicon, Button} from "react-bootstrap";
import _ from "lodash";

export default class ClusterService extends React.Component {
    static propTypes = {
        services: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
        onShowDetail: React.PropTypes.func.isRequired,
        onShowNodeDetail: React.PropTypes.func.isRequired,
    }

    render() {
        return (
            <Table hover={true} striped={true} bordered={true}>
                <thead>
                <tr>
                    <th>Service UUID</th>
                    <th>Service name</th>
                    <th>Service version</th>
                    <th>Service vendor</th>
                    <th>Node UUID</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    _.sortBy(this.props.services, 'name').map((service, index) => {
                        return (
                            <tr key={index}>
                                <td>{service.serviceUuid}</td>
                                <td>{service.name}</td>
                                <td>{service.version}</td>
                                <td>{service.vendor}</td>
                                <td>{service.nodeUuid} <Button bsSize="xsmall" onClick={() => this.props.onShowNodeDetail(service.nodeUuid)}>
                                        <Glyphicon glyph="download" className="darkgreen"/>node detail
                                    </Button></td>
                                <td>
                                    <Button bsSize="xsmall" onClick={() => this.props.onShowDetail(service.serviceUuid)}>
                                        <Glyphicon glyph="download" className="darkgreen"/>detail
                                    </Button>
                                </td>
                            </tr>
                        );
                    })
                }
                </tbody>
            </Table>
        );
    }

    renderNodeServices(node) {
        console.log(node.services)
        if (!node.services) {
            return null;
        }

        let keys = Object.keys(node.services);
        return (
            <span>
            {keys.map((serviceUuid, index) => {
                return (
                    <span key={index}>
                        <Button bsSize="xsmall" onClick={() => this.props.onShowServiceDetail(serviceUuid)}>
                            <Glyphicon glyph="download" className="darkgreen"/>{node.services[serviceUuid]}
                        </Button>{" "}
                    </span>
                )
            })}
            </span>

        );
    }
}