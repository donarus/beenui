import React from "react";
import {Table, Glyphicon, Button} from "react-bootstrap";

export default class ClusterNodeDetail extends React.Component {
    static propTypes = {
        node: React.PropTypes.object.isRequired,
        onShowServiceDetail: React.PropTypes.func.isRequired
    }

    render() {
        return (
            <Table hover={true} striped={true} bordered={true}>
                <tbody>
                <tr>
                    <th>UUID</th>
                    <td>{this.props.node.nodeUuid}</td>
                </tr>
                <tr>
                    <th>Startup time</th>
                    <td>{new Date(this.props.node.startupTime).toLocaleString()}</td>
                </tr>
                <tr>
                    <th>Hostname</th>
                    <td>{this.props.node.hostname}</td>
                </tr>
                <tr>
                    <th>IP addresses</th>
                    <td>{this.props.node.ipAddresses.map((addr, index) => {
                        return (<span key={index}>{addr}<br/></span>);
                    })}</td>
                </tr>
                <tr>
                    <th>Running services</th>
                    <td>{this.renderNodeServices(this.props.node)}</td>
                </tr>
                <tr>
                    <th>RAW</th>
                    <td>
                        <pre>{this.props.node.rawNode}</pre>
                    </td>
                </tr>
                </tbody>
            </Table>
        );
    }

    renderNodeServices(node) {
        console.log(node.services)
        if (!node.services) {
            return null;
        }

        let keys = Object.keys(node.services);
        return (
            <span>
            {keys.map((serviceUuid, index) => {
                return (
                    <span key={index}>
                        <Button bsSize="xsmall" onClick={() => this.props.onShowServiceDetail(serviceUuid)}>
                            <Glyphicon glyph="download" className="darkgreen"/>{node.services[serviceUuid]}
                        </Button>{" "}
                    </span>
                )
            })}
            </span>
        );
    }
}