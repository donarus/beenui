import React from "react";
import {Table, Button, Glyphicon} from "react-bootstrap";

export default class ClusterServiceDetail extends React.Component {
    static propTypes = {
        service: React.PropTypes.object.isRequired,
        onShowNodeDetail: React.PropTypes.func.isRequired,
    }

    render() {
        return (
            <Table hover={true} striped={true} bordered={true}>
                <tbody>
                <tr>
                    <th>UUID</th>
                    <td>{this.props.service.serviceUuid}</td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td>{this.props.service.name}</td>
                </tr>
                <tr>
                    <th>Version</th>
                    <td>{this.props.service.version}</td>
                </tr>
                <tr>
                    <th>Vendor</th>
                    <td>{this.props.service.vendor}</td>
                </tr>
                <tr>
                    <th>Node</th>
                    <td>{this.props.service.nodeUuid} <Button bsSize="xsmall"
                                                   onClick={() => this.props.onShowNodeDetail(this.props.service.nodeUuid)}>
                        <Glyphicon glyph="download" className="darkgreen"/>node detail
                    </Button></td>
                </tr>
                <tr>
                    <th>Description</th>
                    <td>{this.props.service.description}</td>
                </tr>
                <tr>
                    <th>RAW</th>
                    <td><pre>{this.props.service.rawServiceInfo}</pre></td>
                </tr>
                </tbody>
            </Table>
        );
    }
}