import React from "react";
import {Table, Glyphicon, Button} from "react-bootstrap";

export default class ClusterNodes extends React.Component {
    static propTypes = {
        nodes: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
        onShowDetail: React.PropTypes.func.isRequired,
        onShowServiceDetail: React.PropTypes.func.isRequired,
    }

    render() {
        return (
            <Table hover={true} striped={true} bordered={true}>
                <thead>
                <tr>
                    <th>Node UUID</th>
                    <th>Startup time</th>
                    <th>Running services</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    this.props.nodes.map((node, index) => {
                        return (
                            <tr key={index}>
                                <td>{node.nodeUuid}</td>
                                <td>{new Date(node.startupTime).toLocaleString()}</td>
                                <td>{this.renderNodeServices(node)}</td>
                                <td>
                                    <Button bsSize="xsmall" onClick={() => this.props.onShowDetail(node.nodeUuid)}>
                                        <Glyphicon glyph="download" className="darkgreen"/>detail
                                    </Button>
                                </td>
                            </tr>
                        );
                    })
                }
                </tbody>
            </Table>
        );
    }

    renderNodeServices(node) {
        console.log(node.services)
        if (!node.services) {
            return null;
        }

        let keys = Object.keys(node.services);
        return (
            <span>
            {keys.map((serviceUuid, index) => {
                return (
                    <span key={index}>
                        <Button bsSize="xsmall" onClick={() => this.props.onShowServiceDetail(serviceUuid)}>
                            <Glyphicon glyph="download" className="darkgreen"/>{node.services[serviceUuid]}
                        </Button>{" "}
                    </span>
                )
            })}
            </span>

        );
    }
}