import React from "react";
import {Panel, Table} from "react-bootstrap";

export default class LogConsole extends React.Component {
    static propTypes = {
        logs: React.PropTypes.arrayOf(React.PropTypes.array).isRequired,
        header: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),
        style: React.PropTypes.string,
    }

    static defaultProps = {
        header: <h3>Logs</h3>,
        style: "success"
    }

    render() {
        return (
            <Panel header={this.props.header} bsStyle={this.props.style}>
                <Table style={{height: "100%", overflow: "scroll"}}>
                    <thead>
                    <tr>
                        <th>Timestamp</th>
                        <th>Log Level</th>
                        <th>Message</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.logs.map((logMessage, index) => {
                        return (
                            <tr key={index}>
                                <td>{new Date(logMessage.timestamp).toUTCString()}</td>
                                <td>{logMessage.logLevel}</td>
                                <td>{logMessage.message}</td>
                            </tr>
                        );
                    })}
                    </tbody>
                </Table>
            </Panel>
        );
    }
}