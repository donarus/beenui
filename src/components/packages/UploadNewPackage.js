import React from "react";
import FileUpload from "react-fileupload";
import {beenApi} from "../../services/BeenApi";
import {Panel, Button, FormControl, ProgressBar} from "react-bootstrap";

export default class UploadNewPackage extends React.Component {
    static propTypes = {
        uploadUrl: React.PropTypes.string.isRequired,
        onUpload: React.PropTypes.func.isRequired,
        header: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),
        style: React.PropTypes.string,
    }

    static defaultProps = {
        header: <h3>Upload new package</h3>,
        style: "success"
    }

    constructor(props) {
        super(props);
        this.state = {
            fileSelectedForUpload: "",
            progressPercent: 0,
            uplading: false,
            processingOnServer: false
        };
    }

    resetState() {
        this.setState({
            fileSelectedForUpload: "",
            progressPercent: 0,
            uploading: false,
            processingOnServer: false
        })
    }

    render() {
        const that = this
        const options = {
            baseUrl: this.props.uploadUrl,
            fileFieldName: 'bpk',
            uploadSuccess: function (resp) {
                that.resetState()
                beenApi.alertMessages(resp.messages)
                that.props.onUpload(resp.data.groupId, resp.data.artifactId, resp.data.version)
            },
            uploadError: function (err) {
                that.resetState()
                beenApi.alertMessages([{logLevel: "ERROR", message: "upload failed: " + err.toString()}])
            },
            uploadFail: function (resp) {
                that.resetState()
                beenApi.alertMessages(resp.messages)
            },
            chooseFile: function (files) {
                let filename = "";
                if (files) {
                    if (typeof files === 'string') {
                        filename = files
                    } else {
                        filename = files[0].name
                    }
                }
                that.setState({
                    ...that.state,
                    fileSelectedForUpload: filename
                })
            },
            doUpload: function (files, mill) {
                that.setState({
                    ...that.state,
                    uploading: true
                })
            },
            uploading(progress) {
                let progressPercent = (progress.loaded / progress.total) * 100
                if (progressPercent === Infinity) {
                    progressPercent = 100;
                }
                progressPercent = Math.ceil(progressPercent)
                if (progressPercent === 100) {
                    that.setState({
                        ...that.state,
                        processingOnServer: true,
                        uploading: false
                    })
                }
                that.setState({
                    ...that.state,
                    progressPercent: progressPercent
                })
            }
        }

        let chooser = null;
        let statusPart = null;
        let uploadButton = null;
        if (this.state.fileSelectedForUpload === "") {
            chooser = <Button ref="chooseBtn">choose bpk archive to upload</Button>;
            statusPart = "";
            uploadButton = "";
        } else if (this.state.uploading) {
            chooser = <Button ref="chooseBtn" disabled>choose bpk archive to upload</Button>;
            statusPart = (<ProgressBar now={this.state.progressPercent} label={`${this.state.progressPercent}%`}/>);
            uploadButton = <Button ref="uploadBtn" disabled>upload</Button>;
        } else if (this.state.processingOnServer) {
            chooser = <Button ref="chooseBtn" disabled>choose bpk archive to upload</Button>;
            statusPart = (<ProgressBar active now={100} label="processing on server..."/>)
            uploadButton = <Button ref="uploadBtn" disabled>upload</Button>;
        } else {
            chooser = <Button ref="chooseBtn">choose bpk archive to upload</Button>;
            statusPart = (<FormControl type="text" value={this.state.fileSelectedForUpload} disabled/>)
            uploadButton = <Button ref="uploadBtn">upload</Button>;
        }

        return (
            <Panel header={this.props.header} bsStyle={this.props.style}>
                <FileUpload options={options}>
                    {chooser}
                    {statusPart}
                    {uploadButton}
                </FileUpload>
            </Panel>
        );
    }
}


