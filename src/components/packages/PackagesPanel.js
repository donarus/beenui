import React from "react";
import {Panel, Table, Glyphicon, Button} from "react-bootstrap";

export default class PackagesPanel extends React.Component {
    static propTypes = {
        packages: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
        onDelete: React.PropTypes.func.isRequired,
        onCreateDownloadUrl: React.PropTypes.func.isRequired,
        onShowDetail: React.PropTypes.func.isRequired,
        header: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),
        style: React.PropTypes.string,
    }

    static defaultProps = {
        header: <h3>Packages</h3>,
        style: "success"
    }

    render() {
        return (
            <Panel header={this.props.header} bsStyle={this.props.style}>
                <Table striped bordered condensed hover>
                    <thead>
                    <tr>
                        <th>GroupId</th>
                        <th>ArtifactId</th>
                        <th>Version</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.props.packages.map((obj, index) => {
                            return (
                                <tr key={index}>
                                    <td>{obj.groupId}</td>
                                    <td>{obj.artifactId}</td>
                                    <td>{obj.version}</td>
                                    <td>
                                        <Button bsSize="xsmall"
                                                onClick={() => this.props.onShowDetail(obj.groupId, obj.artifactId, obj.version)}>
                                            <Glyphicon glyph="download" className="darkgreen"/>detail
                                        </Button>
                                        <a href={this.props.onCreateDownloadUrl(obj.groupId, obj.artifactId, obj.version)}>
                                            <Button bsSize="xsmall"><Glyphicon glyph="download" className="darkblue"/>
                                                download
                                            </Button>
                                        </a>
                                        <Button bsSize="xsmall"
                                                onClick={() => this.props.onDelete(obj.groupId, obj.artifactId, obj.version)}>
                                            <Glyphicon glyph="remove-circle" className="darkred"/>delete
                                        </Button>
                                    </td>
                                </tr>
                            );
                        })
                    }
                    </tbody>
                </Table>
            </Panel>
        );
    }
}