import React from "react";
import {Panel, Table, Glyphicon, Button} from "react-bootstrap";

export default class PackageDetail extends React.Component {
    static propTypes = {
        bpkDescriptor: React.PropTypes.object.isRequired,
        header: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),
        onRunTaskAsIs: React.PropTypes.func.isRequired,
        style: React.PropTypes.string,
    }

    static defaultProps = {
        style: "success"
    }

    render() {
        let header = this.props.header;
        if (!header) {
            header = <h3>
                Package: {this.props.bpkDescriptor.bpkId.groupId}:{this.props.bpkDescriptor.bpkId.artifactId}:{this.props.bpkDescriptor.bpkId.version}</h3>;
        }

        const bd = this.props.bpkDescriptor;
        return (
            <Panel header={header} bsStyle={this.props.style}>
                <Table striped bordered condensed hover>
                    <tbody>
                    <tr>
                        <th>groupId</th>
                        <td>{bd.bpkId.groupId}</td>
                    </tr>
                    <tr>
                        <th>artifactId</th>
                        <td>{bd.bpkId.artifactId}</td>
                    </tr>
                    <tr>
                        <th>version</th>
                        <td>{bd.bpkId.version}</td>
                    </tr>
                    <tr>
                        <th>description</th>
                        <td>{bd.description}</td>
                    </tr>

                    <tr>
                        <th colSpan="2" style={{color: "red"}}><b>Runnable benchmarks</b></th>
                    </tr>

                    {bd.taskDescriptorTemplates.map((td, index) => {
                        return ([
                                <tr key={index}>
                                    <th>name</th>
                                    <td>{td.name}{'   '}
                                        <Button bsSize="xsmall"
                                                onClick={() => this.props.onRunTaskAsIs(td)}>
                                            <Glyphicon glyph="play" className="darkgreen"/>run
                                        </Button></td>
                                </tr>,
                                <tr>
                                    <th>class</th>
                                    <td>
                                        <b>{td["@class"]}</b>
                                    </td>
                                </tr>]
                        )
                    })}
                    </tbody>
                </Table>
            </Panel>
        );
    }
}


