import React from "react";
import {LineChart, Line, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer} from "recharts";
import {Panel, Tabs, Tab} from "react-bootstrap";

class CPUChart extends React.Component {

    render() {
        return (
            <ResponsiveContainer style={{height: "100%"}}>
                <LineChart data={this.props.data}
                           margin={{top: 5, right: 20, left: 20, bottom: 5}}>
                    <XAxis dataKey="time"/>
                    <YAxis/>
                    <Legend />
                    <Tooltip/>
                    <Line type="monotone" dataKey="pv" stroke="#8884d8"/>
                    <Line type="monotone" dataKey="uv" stroke="#82ca9d"/>
                    <Line type="monotone" dataKey="ua" stroke="#82ca9d"/>
                    <Line type="monotone" dataKey="ub" stroke="#82ca9d"/>
                    <Line type="monotone" dataKey="uc" stroke="#82ca9d"/>
                    <Line type="monotone" dataKey="ud" stroke="#82ca9d"/>
                    <Line type="monotone" dataKey="ue" stroke="#82ca9d"/>
                    <Line type="monotone" dataKey="uf" stroke="#82ca9d"/>
                    <Line type="monotone" dataKey="ug" stroke="#82ca9d"/>
                    <Line type="monotone" dataKey="uh" stroke="#82ca9d"/>
                    <Line type="monotone" dataKey="amt" stroke="#82da9d"/>
                </LineChart>
            </ResponsiveContainer>
        );
    }
}

export default class SimpleLineChart extends React.Component {
    static propTypes = {
        data: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
        header: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),
        style: React.PropTypes.string,
        scrollable: React.PropTypes.bool
    }

    static defaultProps = {
        header: <h3>Usage</h3>,
        style: "success",
        scrollable: false
    }

    render() {
        return (
            <Panel header={this.props.header} bsStyle={this.props.style}>

                <Tabs defaultActiveKey={1} animation={false} id="noanim-tab-example">
                    <Tab eventKey={1} title="Tab 1">
                        <div style={{height: "300px"}}><CPUChart data={this.props.data}/></div>
                    </Tab>
                    <Tab eventKey={2} title="Tab 2">Tab 2 content</Tab>
                    <Tab eventKey={3} title="Tab 3" disabled>Tab 3 content</Tab>
                </Tabs>

            </Panel>
        );
    }
}