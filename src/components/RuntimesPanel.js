import React from "react";
import {Table, Panel} from "react-bootstrap";
import Scrollable from "./Scrollable";


export default class RuntimesPanel extends React.Component {
    static propTypes = {
        colsToHide: React.PropTypes.arrayOf(React.PropTypes.string),
        remapColumnLabels: React.PropTypes.object,
        data: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
        header: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),
        style: React.PropTypes.string,
        scrollable: React.PropTypes.bool
    }

    static defaultProps = {
        colsToHide: [],
        remapColumnLabels: {},
        header: <h3>Runtimes</h3>,
        style: "success",
        scrollable: false
    }

    colsMapping = {
        runtimeId: "Runtime ID",
        hostname: "Hostname",
        port: "Port",
        type: "Type",
        system: "System",
        hardwareInfo: "Hardware info",
        exclusive: "Exclusive",
        tasks: "Tasks",
        cpuLoad: "CPU load",
        freeMemory: "Free memory",
        state: "State"
    }

    getValidCols() {
        var cols = {...this.colsMapping};
        for (let colToHide of this.props.colsToHide) {
            delete cols[colToHide];
        }
        return cols;
    }

    generateHeader(cols) {
        return (
            <thead>
            <tr>
                {Object.keys(cols).map((key) =>
                    <th key={key}>{this.props.remapColumnLabels[key] || cols[key]}</th>
                )}
            </tr>
            </thead>
        );
    }

    generateBody(cols, allData) {
        return (
            <tbody>
            {allData.map((data, index) => {
                return (<tr key={index}>
                    {Object.keys(cols).map((key) =>
                        <td key={key}>{data[key]}</td>
                    )}
                </tr>);
            })}
            </tbody>
        );
    }


    render() {
        const cols = this.getValidCols();

        const table = <Table responsive>
            {this.generateHeader(cols)}
            {this.generateBody(cols, this.props.data)}
        </Table>
        return (
            <Panel header={this.props.header} bsStyle={this.props.style}>
                {
                    this.props.scrollable
                        ? <Scrollable height="200px">{table}</Scrollable>
                        : table
                }
            </Panel>
        );
    }
}