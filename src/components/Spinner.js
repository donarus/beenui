import React from "react";

export default class Spinner extends React.Component {
    render() {
        return (
            <div className="text-center col-md-4 col-md-offset-4 spinner"></div>
        );
    }
}