import React from "react";
import {Link} from "react-router";
import {Navbar, Nav} from "react-bootstrap";

const NavItem = React.createClass({
    render() {
        return (
            <li role="presentation"><Link to={this.props.to}>{this.props.children}</Link></li>
        )
    }
})

const ActionItem = React.createClass({
    render() {
        return (
            <li role="presentation"><Link to="/" onClick={this.props.action}>{this.props.children}</Link></li>
        )
    }
})

export default React.createClass({
    render() {
        return (
            <div>
                <Navbar collapseOnSelect className="text-center">
                    <Navbar.Header>
                        <Navbar.Brand>
                            <Link to={this.props.connected ? "/homepage" : "/login"}>Been UI</Link>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        {this.props.connected
                            ?
                            <Nav>
                                <NavItem to="/packages">Packages</NavItem>
                                <NavItem to="/benchmarks-and-tasks">Benchmarks & Tasks</NavItem>
                                <NavItem to="/cluster">Cluster</NavItem>
                                <ActionItem action={this.props.logoutAction}>Logout</ActionItem>
                            </Nav>
                            :
                            <Nav>
                                <NavItem to="/login">Login</NavItem>
                            </Nav>
                        }
                    </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
})