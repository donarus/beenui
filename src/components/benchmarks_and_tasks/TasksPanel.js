import React from "react";
import {Panel, Table, Glyphicon, Button} from "react-bootstrap";

export default class TasksPanel extends React.Component {
    static propTypes = {
        tasks: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
        onKill: React.PropTypes.func.isRequired,
        onShowTaskDetail: React.PropTypes.func.isRequired,
        header: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),
        style: React.PropTypes.string,
    }

    static defaultProps = {
        header: <h3>Benchmarks</h3>,
        style: "success"
    }

    render() {
        return (
            <Panel header={this.props.header} bsStyle={this.props.style}>
                <Table striped bordered condensed hover>
                    <thead>
                    <tr>
                        <th><font className="red">Task ID</font> / <font className="orange">TaskContext ID</font> /
                            <font className="darkred">Benchmark ID</font></th>
                        <th>Task</th>
                        <th>State</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.props.tasks.map((obj, index) => {
                            return (
                                <tr key={index}>
                                    <td>
                                        <font className="red">{obj.taskUuid}</font>
                                        <Button bsSize="xsmall"
                                                onClick={() => this.props.onShowTaskDetail(obj.taskUuid)}>
                                            <Glyphicon glyph="download" className="darkgreen"/>detail
                                        </Button>
                                        <br/>
                                        <font className="orange">{obj.taskContextUuid}</font><br/>
                                        <font className="darkred">{obj.benchmarkUuid}</font>
                                    </td>
                                    <td><strong>{obj.taskDescriptor.name}</strong>{" "}
                                        ({obj.bpkId.artifactId}-{obj.bpkId.version})
                                    </td>
                                    <td>{obj.taskState}</td>
                                    <td>
                                        <Button bsSize="xsmall"
                                                onClick={() => this.props.onKill(obj.taskUuid)}>
                                            <Glyphicon glyph="remove-circle" className="darkred"/>kill
                                        </Button>
                                    </td>
                                </tr>
                            );
                        })
                    }
                    </tbody>
                </Table>
            </Panel>
        );
    }
}