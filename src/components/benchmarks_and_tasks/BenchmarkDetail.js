import React from "react";
import {Table, Panel, Glyphicon, Button} from "react-bootstrap";
import Spinner from "../Spinner";
import _ from "lodash";
import {common} from "../Common";

export default class BenchmarkDetail extends React.Component {
    static propTypes = {
        benchmark: React.PropTypes.object.isRequired
    }


    static propTypes = {
        benchmark: React.PropTypes.object.isRequired,
        onKillTask: React.PropTypes.func.isRequired,
        onKillTaskContext: React.PropTypes.func.isRequired,
        onKillBenchmark: React.PropTypes.func.isRequired,
        onShowTaskDetail: React.PropTypes.func.isRequired
    }

    constructor(options) {
        super(options)
        this.state = {expanded: []}
    }

    renderTaskRow(task, index) {
        let killButton = null;
        let detailButton = null;
        if (task.taskUuid) {
            detailButton = (
                <Button bsSize="xsmall" onClick={() => this.props.onShowTaskDetail(task.taskUuid)}>
                    <Glyphicon glyph="download" className="darkgreen"/>detail
                </Button>
            );
            if (task.taskState === "RUNNING" || task.taskState === "REQUESTED") {
                killButton = (
                    <Button bsSize="xsmall"
                            onClick={() => this.props.onKillTask(task.taskUuid)}>
                        <Glyphicon glyph="remove-circle" className="darkred"/>kill
                    </Button>
                );
            }
        }
        return (
            <tr key={index}>
                <td>{task.taskName}</td>
                <td>{common.colorizeTaskStatus(task.taskState)}</td>
                <td>{task.taskUuid}</td>
                <td>{common.toDateString(task.start)}</td>
                <td>{common.toDateString(task.end)}</td>
                <td>
                    {detailButton}
                    {killButton}
                </td>
            </tr>
        )
    }

    renderContextFinalizerRow(taskContext) {
        if (taskContext.finalizerTask) {
            let killButton = null;
            let detailButton = null;
            if (taskContext.finalizerTask.taskUuid) {
                detailButton = (
                    <Button bsSize="xsmall"
                            onClick={() => this.props.onShowTaskDetail(taskContext.finalizerTask.taskUuid)}>
                        <Glyphicon glyph="download" className="darkgreen"/>detail
                    </Button>
                );
                if (taskContext.finalizerTask.taskState === "RUNNING" || taskContext.finalizerTask.taskState === "REQUESTED") {
                    killButton = (
                        <Button bsSize="xsmall"
                                onClick={() => this.props.onKillTask(taskContext.finalizerTask.taskUuid)}>
                            <Glyphicon glyph="remove-circle" className="darkred"/>kill
                        </Button>
                    );
                }
            }
            return (
                <tr>
                    <td>{taskContext.finalizerTask.taskName}</td>
                    <td>{common.colorizeTaskStatus(taskContext.finalizerTask.taskState)}</td>
                    <td>{taskContext.finalizerTask.taskUuid}</td>
                    <td>{common.toDateString(taskContext.finalizerTask.start)}</td>
                    <td>{common.toDateString(taskContext.finalizerTask.end)}</td>
                    <td>
                        {detailButton}
                        {killButton}
                    </td>
                </tr>
            )
        } else {
            return null;
        }
    }

    renderTasksTable(contextPos, context) {
        if (_.indexOf(this.state.expanded, contextPos) >= 0) {
            if (context.tasks) {
                return (
                    <tr>
                        <td colSpan="4" style={{'padding': '0px', 'paddingLeft': '20px'}}>

                            <Table hover={true} striped={true} bordered={true} style={{
                                'borderTop': '0px',
                                'borderRight': '0px',
                                'borderLeft': '3px solid black',
                                'padding': '0px'
                            }}>
                                <thead>
                                <tr>
                                    <th>Task name</th>
                                    <th>Task status</th>
                                    <td>Task id</td>
                                    <td>Start</td>
                                    <td>End</td>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {context.tasks.map((task, index) => this.renderTaskRow(task, index))}
                                {this.renderContextFinalizerRow(context)}
                                </tbody>
                            </Table>
                        </td>
                    </tr>
                );
            } else {
                return (
                    <tr>
                        <td colSpan="3" style={{'padding': '0px', 'paddingLeft': '20px', 'paddingBottom': '0px'}}>
                            <Panel>
                                <Spinner/>
                            </Panel>
                        </td>
                    </tr>
                );
            }
        } else {
            return null;
        }
    }

    showHide(contextPos) {
        let newExpanded = this.state.expanded.slice();
        if (_.indexOf(this.state.expanded, contextPos) >= 0) {
            // is expanded
            _.pull(newExpanded, contextPos);
        } else {
            newExpanded = [...newExpanded, contextPos];
        }
        this.setState({...this.state, expanded: newExpanded});
    }

    renderTaskContextRow(index, taskContext) {
        let killButton = null;
        if (taskContext.state === "RUNNING") {
            killButton = (
                <Button bsSize="xsmall"
                        onClick={() => this.props.onKillTaskContext(taskContext.taskContextUuid)}>
                    <Glyphicon glyph="remove-circle" className="darkred"/>kill
                </Button>
            );
        }
        return (
            <tr>
                <td>{taskContext.taskContextName} <a href="#"
                                                     onClick={() => this.showHide(index)}>(show
                    tasks|hide tasks)</a></td>
                <td>{common.colorizeTaskStatus(taskContext.state)}</td>
                <td>{killButton}</td>
                <td>{taskContext.taskContextUuid}</td>
            </tr>
        );
    }

    renderTaskContextsTable(bench) {
        if (bench.taskContexts) {
            return (
                <Table hover={true} striped={true} bordered={true}>
                    <tbody>
                    <tr>
                        <th>Task Context name</th>
                        <th>Task Context status</th>
                        <th>Actions</th>
                        <th>Task Context id</th>
                    </tr>
                    {bench.taskContexts.map((tc, index) => {
                        return ([this.renderTaskContextRow(index, tc), this.renderTasksTable(index, tc)])
                    })}
                    </tbody>
                </Table>
            );
        } else {
            return (
                <Panel>
                    <Spinner/>
                </Panel>
            );
        }
    }

    render() {
        const bench = this.props.benchmark;
        if (!bench) {
            return (
                <Panel>
                    <Spinner/>
                </Panel>
            );
        } else {
            let killButton = null;
            if (bench.state === "RUNNING" || bench.state === "BEING_GENERATED") {
                killButton = (
                    <Button bsSize="xsmall"
                            onClick={() => this.props.onKillBenchmark(bench.benchmarkUuid)}>
                        <Glyphicon glyph="remove-circle" className="darkred"/>kill
                    </Button>
                );
            }
            return (
                <Panel>
                    <Table hover={true} striped={true} bordered={true}>
                        <tbody>
                        <tr>
                            <th>Benchmark ID</th>
                            <td>{bench.benchmarkUuid}</td>
                        </tr>
                        <tr>
                            <th>Benchmark name</th>
                            <td>{bench.benchmarkName}</td>
                        </tr>
                        <tr>
                            <th>Benchmark state</th>
                            <td>{common.colorizeTaskStatus(bench.state)}</td>
                        </tr>
                        <tr>
                            <th>BpkID</th>
                            <td>{bench.bpkId.artifactId}-{bench.bpkId.version}</td>
                        </tr>
                        <tr>
                            <th>Start</th>
                            <td>{common.toDateString(bench.start)}</td>
                        </tr>
                        <tr>
                            <th>End</th>
                            <td>{common.toDateString(bench.end)}</td>
                        </tr>
                        <tr>
                            <th>Actions</th>
                            <td>{killButton}</td>
                        </tr>
                        </tbody>
                    </Table>


                    {this.renderGenerator(bench.generatorTask)}

                    {this.renderBenchmarkFinalizer(bench.finalizerTask)}


                    <strong className="red">
                        execution plan
                    </strong>


                    {this.renderTaskContextsTable(bench)}

                </Panel>
            );
        }
    }

    renderBenchmarkFinalizer(finalizer) {
        if (finalizer) {
            return (
                <span>
                    <strong className="red">
                        benchmark finalizer
                    </strong>
                    <Table hover={true} striped={true} bordered={true}>
                        <thead>
                            <tr>
                                <th>Task name</th>
                                <th>Task status</th>
                                <td>Task id</td>
                                <td>Start</td>
                                <td>End</td>
                                <td>Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderTaskRow(finalizer)}
                        </tbody>
                    </Table>
                </span>
            );
        }
    }

    renderGenerator(generator) {
        if (generator) {
            return (
                <span>
                    <strong className="red">
                        benchmark generator
                    </strong>
                    <Table hover={true} striped={true} bordered={true}>
                        <thead>
                            <tr>
                                <th>Task name</th>
                                <th>Task status</th>
                                <td>Task id</td>
                                <td>Start</td>
                                <td>End</td>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderTaskRow(generator)}
                        </tbody>
                    </Table>
                </span>
            );
        }
    }
}