import React from "react";
import {Table, Panel, Checkbox, Button, Glyphicon} from "react-bootstrap";
import Spinner from "../Spinner";
import {common} from "../Common";

export default class TaskDetail extends React.Component {
    static propTypes = {
        task: React.PropTypes.object.isRequired,
        logs: React.PropTypes.array,
        onShowNodeDetail: React.PropTypes.func.isRequired,
        onShowBenchmarkDetail: React.PropTypes.func.isRequired,
        onCreateDownloadResultsUrl: React.PropTypes.func.isRequired
    }

    constructor(options) {
        super(options)
        this.state = {
            showLoggerName: false,
            showThreadName: false,
            showTimeStamp: false,
            showErrorTrace: true,
            showTaskDescriptor: false
        }
    }

    renderLogs() {
        if (!this.props.logs) {
            return (
                <Panel>
                    <Spinner/>
                </Panel>
            );
        } else {
            return (
                <Panel>
                    <strong>display:</strong><br/>
                    <Checkbox inline checked={this.state.showLoggerName} readOnly onChange={(event) => this.setState({
                        ...this.state,
                        showLoggerName: !this.state.showLoggerName
                    })}>logger</Checkbox>
                    <Checkbox inline checked={this.state.showThreadName} readOnly onChange={(event) => this.setState({
                        ...this.state,
                        showThreadName: !this.state.showThreadName
                    })}>thread</Checkbox>
                    <Checkbox inline checked={this.state.showTimeStamp} readOnly onChange={(event) => this.setState({
                        ...this.state,
                        showTimeStamp: !this.state.showTimeStamp
                    })}>timestamp</Checkbox>
                    <Checkbox inline checked={this.state.showErrorTrace} readOnly onChange={(event) => this.setState({
                        ...this.state,
                        showErrorTrace: !this.state.showErrorTrace
                    })}>error trace</Checkbox>
                    <hr/>


                    <Table>
                        {this.props.logs.map((log, index) => {
                            return (<tr key={index}>
                                <td>{this.formatLogMessage(log)}</td>
                            </tr>)
                        })}
                    </Table>
                </Panel>
            );
        }
    }

    formatLogMessage(logMessage) {
        let info = "";
        if (this.state.showLoggerName) {
            info = logMessage.name;
        }
        if (this.state.showThreadName) {
            if (info)
                info += ", ";
            info = `${info}${logMessage.threadName}`;
        }

        if (info)
            info = ` [${info}]`;

        if (this.state.showTimeStamp)
            info = ` ${info} ${new Date(logMessage.timestamp).toLocaleString()}`;

        const message = `${info} ${logMessage.message}`;

        let errorTrace = this.state.showErrorTrace ? logMessage.errorTrace : "";
        if (errorTrace) {
            errorTrace = (<span><br/><pre>{errorTrace}</pre></span>)
        }

        return (
            <span>
                <font className={`log-${logMessage.level}`}>{logMessage.level}</font>{" "}{message}{errorTrace}
            </span>
        );
    }

    render() {
        let taskDescriptor = null;
        if (this.state.showTaskDescriptor) {
            taskDescriptor = (<pre>{JSON.stringify(this.props.task.taskDescriptor, null, 2)}</pre>);
        }

        let nodeDetailButton = null;
        if (this.props.task.nodeUuid) {
            nodeDetailButton = (
                <Button bsSize="xsmall" onClick={() => this.props.onShowNodeDetail(this.props.task.nodeUuid)}>
                    <Glyphicon glyph="download" className="darkgreen"/>detail</Button>);
        }
        const benchmarkDetailButton = (
            <Button bsSize="xsmall" onClick={() => this.props.onShowBenchmarkDetail(this.props.task.benchmarkUuid)}>
                <Glyphicon glyph="download" className="darkgreen"/>detail</Button>);

        const downloadResultsButton = (
            <a href={this.props.onCreateDownloadResultsUrl(this.props.task.taskUuid)} target="_blank">
                <Button bsSize="xsmall"><Glyphicon glyph="download" className="darkblue"/>
                    download
                </Button>
            </a>);

        return (
            <span>
            <Table hover={true} striped={true} bordered={true}>
                <tbody>
                <tr>
                    <th>id</th>
                    <td>{this.props.task.taskUuid}</td>
                </tr>
                <tr>
                    <th>task context id</th>
                    <td>{this.props.task.taskContextUuid}</td>
                </tr>
                <tr>
                    <th>benchmark id</th>
                    <td>{this.props.task.benchmarkUuid} {benchmarkDetailButton}</td>
                </tr>
                <tr>
                    <th>bpk id</th>
                    <td>{this.props.task.bpkId.groupId}:{this.props.task.bpkId.artifactId}:{this.props.task.bpkId.version}</td>
                </tr>
                <tr>
                    <th>task type</th>
                    <td>{this.props.task.taskType}</td>
                </tr>
                <tr>
                    <th>state</th>
                    <td>{common.colorizeTaskStatus(this.props.task.taskState)}</td>
                </tr>
                <tr>
                    <th>start</th>
                    <td>{common.toDateString(this.props.task.taskStartTime)}</td>
                </tr>
                <tr>
                    <th>end</th>
                    <td>{common.toDateString(this.props.task.taskEndTime)}</td>
                </tr>
                <tr>
                    <th>runs on node with id</th>
                    <td>{this.props.task.nodeUuid} {nodeDetailButton}
                    </td>
                </tr>
                <tr>
                    <th>task directory</th>
                    <td>{this.props.task.taskDirectory}</td>
                </tr>
                <tr>
                    <th>task exit code</th>
                    <td>{this.props.task.taskExitCode}</td>
                </tr>
                <tr>
                    <th>task descriptor</th>
                    <td>
                        <a href="#" onClick={(event) => this.setState({
                            ...this.state,
                            showTaskDescriptor: !this.state.showTaskDescriptor
                        })}>show/hide</a><br/>
                        {taskDescriptor}
                    </td>
                </tr>


                </tbody>
            </Table>

            <hr/>
            <strong>results: </strong>
            {downloadResultsButton}
            <hr/>

            <strong>logs</strong><br/>
                {this.renderLogs()}
            </span>
        );
    }
}