import React from "react";
import {Panel, Table, Glyphicon, Button} from "react-bootstrap";
import {common} from "../Common";

export default class BenchmarksPanel extends React.Component {
    static propTypes = {
        benchmarks: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
        onKillBenchmark: React.PropTypes.func.isRequired,
        onShowBenchmarkDetail: React.PropTypes.func.isRequired,
        header: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.object]),
        style: React.PropTypes.string,
    }

    static defaultProps = {
        header: <h3>Benchmarks</h3>,
        style: "success"
    }

    renderBenchmarkRow(benchmark, index) {
        let killButton = null;
        if (benchmark.state === "RUNNING" || benchmark.state === "BEING_GENERATED") {
            killButton = (
                <Button bsSize="xsmall"
                        onClick={() => this.props.onKillBenchmark(benchmark.benchmarkUuid)}>
                    <Glyphicon glyph="remove-circle" className="darkred"/>kill
                </Button>
            );
        }
        return (
            <tr key={index}>
                <td>{benchmark.benchmarkUuid}</td>
                <td>{benchmark.benchmarkName}</td>
                <td>{common.toDateString(benchmark.start)}</td>
                <td>{common.toDateString(benchmark.end)}</td>
                <td>{benchmark.bpkId.artifactId}-{benchmark.bpkId.version}</td>
                <td>{benchmark.state}</td>
                <td>
                    <Button bsSize="xsmall"
                            onClick={() => this.props.onShowBenchmarkDetail(benchmark.benchmarkUuid)}>
                        <Glyphicon glyph="download" className="darkgreen"/>detail
                    </Button>
                    {killButton}
                </td>
            </tr>
        );
    }

    render() {
        return (
            <Panel header={this.props.header} bsStyle={this.props.style}>
                <Table striped bordered condensed hover>
                    <thead>
                    <tr>
                        <th>Benchmark ID</th>
                        <th>Benchmark name</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Bpk ID</th>
                        <th>Benchmark state</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.props.benchmarks.map((obj, index) => {
                            return this.renderBenchmarkRow(obj, index);
                        })
                    }
                    </tbody>
                </Table>
            </Panel>
        );
    }
}