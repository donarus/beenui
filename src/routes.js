import React from "react";
import {Route, IndexRoute} from "react-router";
// ROOT COMPONENT
import App from "./App";
// PAGES
import Packages from "./pages/Packages";
import PackageDetail from "./pages/PackageDetail";
import RuntimeDetail from "./pages/RuntimeDetail";
import Cluster from "./pages/Cluster";
import BenchmarksAndTasks from "./pages/BenchmarksAndTasks";
import LoginPage from "./pages/LoginPage";
import NodeDetail from "./pages/NodeDetail";
import ServiceDetail from "./pages/ServiceDetail";
import TaskDetail from "./pages/TaskDetail";
import BenchmarkDetail from "./pages/BenchmarkDetail";
import Error404Page from "./pages/Error404Page";
import HomePage from "./pages/HomePage";

export default (
    <Route path="/" component={App}>
        <IndexRoute component={LoginPage}/>
        <Route path="homepage" component={HomePage}/>
        <Route path="login" component={LoginPage}/>
        <Route path="benchmarks-and-tasks" component={BenchmarksAndTasks}/>
        <Route path="packages" component={Packages}/>
        <Route path="package/:groupId/:artifactId/:version" component={PackageDetail}/>
        <Route path="cluster/node/:nodeUuid" component={NodeDetail}/>
        <Route path="cluster/service/:serviceUuid" component={ServiceDetail}/>
        <Route path="runtime-detail" component={RuntimeDetail}/>
        <Route path="task/:taskId" component={TaskDetail}/>
        <Route path="benchmark/:benchmarkId" component={BenchmarkDetail}/>
        <Route path="cluster" component={Cluster}/>
        <Route path="*" component={Error404Page}/>
    </Route>
);