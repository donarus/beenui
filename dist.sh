#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

echo "Building production version"
npm run build

echo "CLeaning old build"
rm -rf dist
rm -rf dist.zip

echo "Creating 'dist' directory"
mkdir dist

echo "Copying 'build' to 'dist'"
cp -R build dist

echo "Copying 'other files' to 'dist'"
cp -R files/* dist

echo "Generating 'dist.zip' archive"
zip -r dist.zip dist

echo "Hurrey!!! Dist generated!!!"
